﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCiBot.Interpretes.Shared.Models.HttpClients
{
    public interface IHttpClientWIT
    {
        HttpClient HttpClient();
    }
}
