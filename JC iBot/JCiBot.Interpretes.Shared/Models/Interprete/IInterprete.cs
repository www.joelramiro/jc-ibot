﻿// <copyright file="IInterprete.cs" company="JC Company">
// Copyright (c) JC Company. All rights reserved.
// </copyright>

namespace JCiBot.Interpretes.Shared.Models.Interprete
{
    using JCiBot.General.Shared.Models;

    public interface IInterprete
    {
        public string Key { get; }

        Task<ServiceResult<InterpreteOption>> InterpreteAsync(string ChannelKey, InterpreteContent interpreteContent);
    }
}