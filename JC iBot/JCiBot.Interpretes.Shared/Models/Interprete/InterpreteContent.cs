﻿// <copyright file="InterpreteContent.cs" company="JC Company">
// Copyright (c) JC Company. All rights reserved.
// </copyright>

namespace JCiBot.Interpretes.Shared.Models.Interprete
{
    public class InterpreteContent
    {
        public string Text { get; set; }
    }
}
