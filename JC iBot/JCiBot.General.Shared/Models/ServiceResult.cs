﻿// <copyright file="ServiceResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace JCiBot.General.Shared.Models
{
    using System.Collections.Generic;
    using System.Linq;

    public class ServiceResult<T>
    {
        public IReadOnlyDictionary<string, IEnumerable<string>> ValidationMessages { get; protected set; }

        public bool Succeeded { get; protected set; }

        public T Result { get; set; }

        public ServiceResult(T result, bool succeeded, Dictionary<string, IEnumerable<string>> errors)
        {
            this.Result = result;
            this.Succeeded = succeeded;
            this.ValidationMessages = errors;
        }

        public static ServiceResult<T> ErrorResult(string[] errors)
        {
            return new(default, false, new Dictionary<string, IEnumerable<string>> { { string.Empty, errors.Select(e => e) } });
        }

        public static ServiceResult<T> SuccessResult(T result) =>
            new ServiceResult<T>(result, true, new Dictionary<string, IEnumerable<string>>());
    }
}


