﻿using JCiBot.Channels.Shared.Services;
using JCiBot.Interpretes.Shared.Models.HttpClients;
using JCiBot.Interpretes.Shared.Models.Interprete;
using Microsoft.AspNetCore.Mvc;

namespace JCiBOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IChannel channel;
        private readonly IEnumerable<IInterprete> interpretes;

        public TestController(
            IChannel channel
            ,IEnumerable<IInterprete> interpretes)
        {
            this.channel = channel;
            this.interpretes = interpretes;
        }

        [HttpGet("test{text}")]
        public async Task<IActionResult> test(string text)
        {
            var t = this.interpretes.FirstOrDefault(i => i.Key == "WIT");
            var result = await t.InterpreteAsync(string.Empty, new InterpreteContent 
            {
                Text = text,
            });
            return this.Ok(result);
        }

        [HttpPost("Start")]
        public async Task<IActionResult> Start()
        {
            try
            {
                var result = await this.channel.StartChannelAsync();

                if (result.Succeeded)
                {
                    return this.Ok(new
                    {
                        Error = "Service has started"
                    });
                }

                return this.BadRequest(new
                {
                    Error = "Error starting the service"
                });
            }
            catch (Exception)
            {
                return this.BadRequest(new
                {
                    Error = "Internal server error"
                });
            }
        }

        [HttpPost("Stop")]
        public async Task<IActionResult> Stop()
        {
            try
            {
                var result = await this.channel.StopChannelAsync();

                if (result.Succeeded)
                {
                    return this.Ok(new
                    {
                        Error = "Service has stopped"
                    });
                }

                return this.BadRequest(new
                {
                    Error = "Error stopping the service"
                });
            }
            catch (Exception)
            {
                return this.BadRequest(new
                {
                    Error = "Internal server error"
                });
            }
        }
    }
}
