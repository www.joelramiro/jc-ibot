﻿using JCiBot.Interpretes.LUIS.Interprete;
using JCiBot.Interpretes.Shared.Models.Interprete;
using JCiBot.Interpretes.WIT.Interprete;

namespace JCiBot.Api.Services.InterpretesInject
{
    public static class AddInterpreteSection
    {
        public static void Add(WebApplicationBuilder builder)
        {
            builder.Services.AddTransient<IInterprete, LUISInterprete>();
            builder.Services.AddTransient<IInterprete, WITInterprete>();
        }
    }
}
