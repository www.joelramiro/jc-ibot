﻿using JCiBot.Interpretes.Shared.Models.HttpClients;
using JCiBot.Interpretes.WIT.HttpClients;
using System.Net;
using System.Net.Http.Headers;

namespace JC_iBot.Services.HttpClients
{
    public static class AddHttpClientsSection
    {
        public static void Add(WebApplicationBuilder builder)
        {
            var baseUrl = builder.Configuration.GetSection("HttpClients").GetSection("HttpClientWIT")["BaseUrl"];
            var authToken = builder.Configuration.GetSection("HttpClients").GetSection("HttpClientWIT")["AuthToken"];

            builder.Services.AddHttpClient<IHttpClientWIT, HttpClientWIT>(client =>
            {
                client.BaseAddress = new Uri(baseUrl == null ? string.Empty : baseUrl);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
            });
        }
    }
}
