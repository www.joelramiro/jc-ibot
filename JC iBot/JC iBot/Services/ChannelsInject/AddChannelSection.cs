﻿using JCiBot.Channels.Shared.Services;
using JCiBot.Channels.Telegram.Channel;
using JCiBot.Channels.Telegram.ConfigurationFile;
using JCiBot.Channels.Telegram.Core.Intents;
using JCiBot.Channels.Telegram.Models;

namespace JCiBot.Api.Services.ChannelsInject
{
    public static class AddChannelSection
    {
        public static void Add(WebApplicationBuilder builder)
        {
            try
            {
                builder.Services.AddScoped<IIntentResult, PersonnalInformationIntent>();
                builder.Services.AddTransient<IChannel, TelegramChannel>();


#pragma warning disable CS8603 // Posible tipo de valor devuelto de referencia nulo
                _ = builder.Services.AddSingleton<ITelegramChannelConfigurationFile, TelegramChannelConfigurationFile>(c =>
                builder.Configuration.GetSection(nameof(TelegramChannelConfigurationFile))
                .Get<TelegramChannelConfigurationFile>());
#pragma warning restore CS8603 // Posible tipo de valor devuelto de referencia nulo
            }
            catch (Exception e)
            {
                var rr = e.Message;
            }
        }
    }
}
