using JC_iBot.Services.HttpClients;
using JCiBot.Api.Services.ChannelsInject;
using JCiBot.Api.Services.InterpretesInject;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
AddChannelSection.Add(builder);
AddInterpreteSection.Add(builder);

builder.Services.AddHttpClient();
AddHttpClientsSection.Add(builder);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
