﻿using JCiBot.Interpretes.Shared.Models.HttpClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCiBot.Interpretes.WIT.HttpClients
{
    public class HttpClientWIT : IHttpClientWIT
    {
        private readonly HttpClient httpClient;

        public HttpClientWIT(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public HttpClient HttpClient()
        {
            return this.httpClient;
        }
    }
}
