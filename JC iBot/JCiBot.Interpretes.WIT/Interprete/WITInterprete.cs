﻿using JCiBot.General.Shared.Models;
using JCiBot.Interpretes.Shared.Models.HttpClients;
using JCiBot.Interpretes.Shared.Models.Interprete;
using JCiBot.Interpretes.WIT.Models.Response;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JCiBot.Interpretes.WIT.Interprete
{
    public class WITInterprete : IInterprete
    {
        private readonly IHttpClientWIT httpClientWIT;
        private readonly IConfiguration configuration;

        public WITInterprete(
            IHttpClientWIT httpClientWIT, IConfiguration configuration)
        {
            this.httpClientWIT = httpClientWIT;
            this.configuration = configuration;
        }

        public string Key => "WIT";

        public async Task<ServiceResult<InterpreteOption>> InterpreteAsync(string ChannelKey, InterpreteContent interpreteContent)
        {
            try
            {
                var query = HttpUtility.ParseQueryString(string.Empty);
                string version = configuration.GetSection("HttpClients").GetSection("HttpClientWIT")["Version"];

                query["v"] = version;
                query["q"] = interpreteContent.Text;
                string queryString = query.ToString();

                var url = $"message?{queryString}";
                var result = await this.httpClientWIT.HttpClient().GetAsync(url);

                if (result.IsSuccessStatusCode)
                {
                    var response = await result.Content.ReadAsStringAsync();
                    var myJsonObjectResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<Root>(response);

                    if (myJsonObjectResponse.Intents != null && myJsonObjectResponse.Intents.Any())
                    {
                        var jsonIntentResult = myJsonObjectResponse.Intents
                            .OrderByDescending(d => d.Confidence)
                            .FirstOrDefault();

                        if (jsonIntentResult.Confidence > 0.9)
                        {
                            return ServiceResult<InterpreteOption>.SuccessResult(new InterpreteOption 
                            {
                                Jsonalue = response,
                                Key = jsonIntentResult.Name,
                            });
                        }
                        else
                        {
                            return ServiceResult<InterpreteOption>.ErrorResult(new[] { "Cannot interpret the request" });
                        }
                    }
                    else
                    {
                        return ServiceResult<InterpreteOption>.ErrorResult(new[] { "Cannot interpret the request." });
                    }
                }
                else
                {
                    return ServiceResult<InterpreteOption>.ErrorResult(new[] { "Internal server error." });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return ServiceResult<InterpreteOption>.ErrorResult(new[] { "Error with interprete" });
            }
        }
    }
}