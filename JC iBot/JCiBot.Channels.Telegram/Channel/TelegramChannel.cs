﻿// <copyright file="TelegramChannel.cs" company="JC Company">
// Copyright (c) JC Company. All rights reserved.
// </copyright>

namespace JCiBot.Channels.Telegram.Channel
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using global::Telegram.Bot.Exceptions;
    using global::Telegram.Bot.Polling;
    using global::Telegram.Bot.Types.Enums;
    using global::Telegram.Bot.Types;
    using global::Telegram.Bot;
    using JCiBot.Channels.Shared.Services;
    using JCiBot.Channels.Telegram.ConfigurationFile;
    using JCiBot.General.Shared.Models;
    using JCiBot.Interpretes.Shared.Models.Interprete;
    using JCiBot.Channels.Telegram.Models;

    public class TelegramChannel : IChannel
    {
        private readonly ITelegramChannelConfigurationFile telegramChannelConfigurationFile;
        private readonly IEnumerable<IIntentResult> intents;
        private readonly IInterprete interprete;
        private ITelegramBotClient telegramBotClient;
        private CancellationTokenSource cancellationTokenSource;

        public TelegramChannel(
            ITelegramChannelConfigurationFile telegramChannelConfigurationFile
            , IEnumerable<IInterprete> interpretes
            , IEnumerable<IIntentResult> intents)
        {
            this.telegramChannelConfigurationFile = telegramChannelConfigurationFile;
            this.intents = intents;
            this.interprete = interpretes.FirstOrDefault(i => i.Key == "WIT");
            this.telegramBotClient = new TelegramBotClient(this.telegramChannelConfigurationFile.ApiKey);
        }

        public string Key => "TELEGRAM";

        private bool isConnected { get; set; } = false;

        public async Task<bool> IsConnected()
        {
            return this.isConnected;
        }

        public async Task<ServiceResult<bool>> StartChannelAsync()
        {
            if (this.isConnected)
            {
                return ServiceResult<bool>.SuccessResult(true);
            }

            try
            {
                this.cancellationTokenSource = new CancellationTokenSource();
                var receiverOptions = new ReceiverOptions
                {
                    AllowedUpdates = Array.Empty<UpdateType>(), // receive all update types
                };

                this.telegramBotClient.StartReceiving(
                    updateHandler: this.HandleUpdateAsync,
                    pollingErrorHandler: this.HandlePollingErrorAsync,
                    receiverOptions: receiverOptions,
                    cancellationToken: this.cancellationTokenSource.Token);
                this.isConnected = true;
                return ServiceResult<bool>.SuccessResult(true);
            }
            catch (Exception)
            {
                return ServiceResult<bool>.ErrorResult(new[] { "An error occurred initializing the Telegram Bot service" });
            }
        }

        public async Task<ServiceResult<bool>> StopChannelAsync()
        {
            try
            {
                this.cancellationTokenSource.Cancel();
                this.isConnected = false;
                return ServiceResult<bool>.SuccessResult(true);
            }
            catch (Exception)
            {
                return ServiceResult<bool>.ErrorResult(new[] { "An error occurred stopping the Telegram Bot service" });
            }
        }

        async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            // Only process Message updates: https://core.telegram.org/bots/api#message
            if (update.Message is not { } message)
            {
                return;
            }

            // Only process text messages
            if (message.Text is not { } messageText)
            {
                return;
            }

            var chatId = message.Chat.Id;



            Console.WriteLine($"Received a '{messageText}' message in chat {chatId}.");

            var result = await this.interprete.InterpreteAsync(this.Key, new InterpreteContent
            {
                Text = messageText,
            });

            if (result.Succeeded)
            {
                var intent = this.intents.FirstOrDefault(i => i.IntentName == result.Result.Key);

                if (intent != null)
                {
                    await intent.Execute(message, this.cancellationTokenSource, this.telegramBotClient, result.Result.Jsonalue);
                }
                else
                {
                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "An error occurred interpreting the message.",
                        cancellationToken: cancellationToken);
                }
            }
            else
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: "An error occurred interpreting the message.",
                    cancellationToken: cancellationToken);
            }
        }

        Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            var ErrorMessage = exception switch
            {
                ApiRequestException apiRequestException
                    => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };

            Console.WriteLine(ErrorMessage);
            return Task.CompletedTask;
        }
    }
}
