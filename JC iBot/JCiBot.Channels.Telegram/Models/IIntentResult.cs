﻿using JCiBot.General.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot;

namespace JCiBot.Channels.Telegram.Models
{
    public interface IIntentResult
    {
        public string IntentName { get; }

        public Task<ServiceResult<bool>> Execute(Message message, CancellationTokenSource cancellationTokenSource, ITelegramBotClient telegramBotClient, string jsonResult);

        public TypeResult TypeResult { get; }
    }
}