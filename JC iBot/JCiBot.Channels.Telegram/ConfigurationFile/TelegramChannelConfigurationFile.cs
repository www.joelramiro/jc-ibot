﻿// <copyright file="TelegramChannelConfigurationFile.cs" company="JC Company">
// Copyright (c) JC Company. All rights reserved.
// </copyright>

namespace JCiBot.Channels.Telegram.ConfigurationFile
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class TelegramChannelConfigurationFile : ITelegramChannelConfigurationFile
    {
        public string ApiKey { get; set; } = string.Empty;
    }
}
