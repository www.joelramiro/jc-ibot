﻿// <copyright file="ITelegramChannelConfigurationFile.cs" company="JC Company">
// Copyright (c) JC Company. All rights reserved.
// </copyright>

namespace JCiBot.Channels.Telegram.ConfigurationFile
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface ITelegramChannelConfigurationFile
    {
        string ApiKey { get; set; }
    }
}
