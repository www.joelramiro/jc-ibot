﻿using JCiBot.Channels.Telegram.Models;
using JCiBot.General.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace JCiBot.Channels.Telegram.Core.Intents
{
    public class PersonnalInformationIntent : IIntentResult
    {
        public PersonnalInformationIntent()
        {
        }

        public string IntentName => "get_personal_information";

        public TypeResult TypeResult => TypeResult.PlainText;

        public async Task<ServiceResult<bool>> Execute(Message message, CancellationTokenSource cancellationTokenSource, ITelegramBotClient telegramBotClient, string jsonResult)
        {
            try
            {
                Message sentMessage = await telegramBotClient.SendTextMessageAsync(
                    chatId: message.Chat.Id,
                    text: $"Tú eres {message.Chat.FirstName} {message.Chat.LastName}",
                    cancellationToken: cancellationTokenSource.Token);

                return ServiceResult<bool>.SuccessResult(true);
            }
            catch (Exception)
            {
                return ServiceResult<bool>.ErrorResult(new[] { "Internal server error." });
            }
        }
    }
}
