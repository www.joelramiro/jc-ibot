﻿namespace JCiBot.Channels.Shared.Services
{
    using JCiBot.General.Shared.Models;
    using System.Threading.Tasks;

    public interface IChannel
    {
        string Key { get; }

        Task<bool> IsConnected();

        Task<ServiceResult<bool>> StartChannelAsync();

        Task<ServiceResult<bool>> StopChannelAsync();
    }
}
