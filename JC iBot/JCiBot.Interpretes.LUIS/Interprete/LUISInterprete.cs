﻿
// <copyright file="LUISInterprete.cs" company="JC Company">
// Copyright (c) JC Company. All rights reserved.
// </copyright>

namespace JCiBot.Interpretes.LUIS.Interprete
{
    using System.Threading.Tasks;
    using JCiBot.General.Shared.Models;
    using JCiBot.Interpretes.Shared.Models.Interprete;

    public class LUISInterprete : IInterprete
    {
        public string Key => "LUIS";

        public async Task<ServiceResult<InterpreteOption>> InterpreteAsync(string ChannelKey, InterpreteContent interpreteContent)
        {
            return ServiceResult<InterpreteOption>.ErrorResult(new[] { "Not implemented" });
        }
    }
}
